# JAVASCRIPT
## monothread et asynchrone
- exécution d'une seule instruction à la fois via la pile d'exécution : **Call Stack**.
- asynchrone parce que le comportement de certains comportements peuvent être délégués en dehors du moteur JS; pour une page Web, les traitements asynchrones seront délégués au navigateur (qui peut, quant à lui, être multithreads).

## circuit des traitements asynchrone
le circuit par de la **Call Stack**, sort via les APIs système de l'hôte (**Host APIs**), tombe dans la file d'attente des callbacks (**Callback Queue**), puis la boucle d'événements (**Event Loop**) pour revenir dans le moteur JS sur la pile d'exécution.

### Call Stack
- assure le suivi de la chaine d'appel des fonctions en mémorisant la fonction et son contexte d'exécution (variables locales et paramètres d'appel).

### Host APIs
- fournit un système de fonctions et d'objets (API) au moteur JS permettant d'interagir avec le système d'exploitation.
Certaines de ses fonctions sont asynchrones (accès aux ressources système, au DOM ...).
- le moteur JS est informé de l'état de traitement de ses foncitons asynchrone par l'intermédiaire des callbacks.

### Event Loop
- une fois l'appel au callback (lors d'un traitement asynchrone ou survenue d'un événement), le callback est inséré dans la file d'attente des callbacks avant d'être pris en compte par la boucle des événements.

- la boucle des événements surveille la pile d'éxécution. Si cette dernière est vide d'instruction, elle déplace le callback en attente dans la pile des callback vers la pile d'éxécution.

## Les Promises
- une Promise est un objet qui représente la complétion ou l'échec d'une opération asynchrone.