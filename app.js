let thing  = [];
let aThing  = [];
const succesHandler = (params) => {
    console.log(`[succesHandler][isAsync = ${params.isAsync}][thing, aThing][01] : `, thing, thing.length, aThing, aThing.length);
    if ( params.isAsync ) {
        aThing.push(params.msg);
    }
    else {
        thing.push(params.msg);
    }
    console.log(`[succesHandler][isAsync = ${params.isAsync}][thing, aThing][02] : `, thing, thing.length, aThing, aThing.length);
};

const errorHandler = (params) => {
    console.log(`[errorHandler][isAsync = ${params.isAsync}][thing, aThing][01] : `, thing, thing.length, aThing, aThing.length);
    if ( params.isAsync ) {
        aThing.push(params.msg);
    }
    else {
        thing.push(params.msg);
    }
    console.log(`[errorHandler][isAsync = ${params.isAsync}][thing, aThing][02] : `, thing, thing.length, aThing, aThing.length);
};

const doPromise = (isOK, isAsync) => {
    return myFirstPromise(isOK, isAsync);
};

const syncMain = () => {
    console.log(`[syncMain][thing, aThing][01] : `, thing, thing.length, aThing, aThing.length);
    doPromise(false, false).then(succesHandler).catch(errorHandler);
    console.log(`[syncMain][thing, aThing][02] : `, thing, thing.length, aThing, aThing.length);
    doPromise(true, false).then(succesHandler).catch(errorHandler);
    console.log(`[syncMain][thing, aThing][03] : `, thing, thing.length, aThing, aThing.length);
};

const asyncMain = async () => {
    // expected :
    // [asyncMain][thing, aThing][01] : [] 0 [] 0 
    console.log(`[asyncMain][thing, aThing][01] : `, thing, thing.length, aThing, aThing.length);
    // expected :
    // - [myFirstPromise][Promise][before callbacks]
    // - [myFirstPromise][Promise][false][true]
    // - [errorHandler][isAsync = true][thing, aThing][01] : [OK, BAD] 2 [] 0
    // - [errorHandler][isAsync = true][thing, aThing][02] : [OK, BAD] 2 0 [BAD] 1
    await doPromise(false, true).then(succesHandler).catch(errorHandler);
    // expected :
    // - [asyncMain][thing, aThing][02] : [OK, BAD] 2 0 [BAD] 1
    console.log(`[asyncMain][thing, aThing][02] : `, thing, thing.length, aThing, aThing.length);
    // expected :
    // - [myFirstPromise][Promise][before callbacks]
    // - [myFirstPromise][Promise][true][true]
    // - [succesHandler][isAsync = true][thing, aThing][01] : [OK, BAD] 2 [BAD] 1
    // - [succesHandler][isAsync = true][thing, aThing][02] : [OK, BAD] 2 [BAD, OK] 2
    await doPromise(true, true).then(succesHandler).catch(errorHandler);
    // expected :
    // - [asyncMain][thing, aThing][03] : [OK, BAD] 2 [BAD, OK] 2
    console.log(`[asyncMain][thing, aThing][03] : `, thing, thing.length, aThing, aThing.length);
};

syncMain();
console.log(`----------------------------------------------------`);
asyncMain();

