const myFirstPromise     = (isOK, isAsync) => {
    return new Promise ((resolve, reject) => {
        console.log(`[myFirstPromise][Promise][before callbacks]`);
        if ( isOK ) {
            console.log(`[myFirstPromise][Promise][${isOK}][${isAsync}]`);
            resolve({msg:"OK", isAsync:isAsync});
        }
        else {
            console.log(`[myFirstPromise][Promise][${isOK}][${isAsync}]`);
            reject({msg:"BAD", isAsync:isAsync});
        }
    });
};